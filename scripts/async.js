const { province, municipality, endpoint } = require("./constants");
const {
    fetchData,
    getAddressId,
    getAllBarangays,
    writeToCsv,
} = require("./utils");

const getBarangays = async () => {
    try {
        const provinces = await fetchData(endpoint);
        const provinceId = getAddressId(provinces, province);
        const municipalities = await fetchData(`${endpoint}${provinceId}`);
        const municipalityId = getAddressId(municipalities, municipality);
        const barangays = await getAllBarangays(municipalityId);

        writeToCsv(barangays, `data_${municipality}.csv`);
    } catch (error) {
        console.log("Failed to retrieve data.");
    }
};

getBarangays();
