const { province, municipality, endpoint } = require("./constants");
const { getAddressId } = require("./utils");
const request = require("request");

/**
 * Reviver function for JSON.parse(). Removes unused variables
 */
const cleanUp = (name, value) =>
    name === "nameLocal" || name === "displayName" ? undefined : value;

/**
 * Retrieves data from a given API url via HTTP Request
 * @param {string} url - url of the API endpoint
 * @param {function(string, Array):object} callback - function to be called when the HTTP request has been resolved
 */
const getRequest = (url, callback) => {
    const options = {
        url: url,
        method: "GET",
        headers: {
            "Content-Type": "application/json",
            dataType: "json",
        },
    };

    request(options, (err, res, body) => {
        if (callback) {
            try {
                callback(err, JSON.parse(body, cleanUp));
            } catch (error) {
                console.log("Invalid data.");
            }
        }
    });
};

getRequest(endpoint, (error, data) => {
    if (error) {
        console.log("Failed to connect to server.");
        return;
    } else {
        console.log(data.module);
    }

    const provinceId = getAddressId(data.module, province);

    getRequest(`${endpoint}${provinceId}`, (error, data) => {
        if (error) {
            console.log("Failed to connect to server.");
            return;
        } else {
            console.log(data.module);
        }

        const municipalityId = getAddressId(data.module, municipality);

        getRequest(`${endpoint}${municipalityId}`, (error, data) => {
            if (error) {
                console.log("Failed to connect to server.");
                return;
            } else {
                console.log(data.module);
            }
        });
    });
});
