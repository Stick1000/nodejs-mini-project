const { province, municipality, endpoint } = require("./constants");
const { getAddressId, writeToCsv } = require("./utils");
const axios = require("axios");

axios
    .get(endpoint)
    .then((response) => {
        const provinceId = getAddressId(response.data.module, province);

        return axios.get(`${endpoint}${provinceId}`);
    })
    .then((response) => {
        const municipalityId = getAddressId(response.data.module, municipality);

        return axios.get(`${endpoint}${municipalityId}`);
    })
    .then((response) => {
        writeToCsv(response.data.module, `data_${municipality}.csv`);
    })
    .catch(() => console.log("Failed to retrieve data."));
