const { endpoint } = require("./constants");
const axios = require("axios");
const path = require("path");
const { writeToPath } = require("@fast-csv/format");

/**
 * A handy blocking/delay method
 * @param {number} ms - time to delay in milliseconds, default value is 1000 milliseconds (1 second)
 */
const delay = (ms = 1000) => new Promise((resolve) => setTimeout(resolve, ms));

/**
 * Retrieves data from a given API url via Axios
 * @param {string} url - url of the API endpoint
 * @returns {Array} null if there was no response, else an array containing the datapoints
 */
const fetchData = async (url) => {
    const result = await axios.get(url);
    return Array.isArray(result.data.module) ? result.data.module : null;
};

/**
 *  Returns the addressId of a specified `location`.
 * @param {Array} data - an object array to lookup addressIds from
 * @param {string} location - location to be searched
 */
const getAddressId = (data, location) =>
    data.find((e) => e.name === location).id;

/**
 *  Returns all the addressIds of subaddresses given a dataset.
 * @param {Array} data - an object array to lookup addressIds from
 * @returns {string[]} array containing addressIds
 */
const getAddressIds = (data) => data.map((e) => e.id);

/**
 * Retrieves all barangays given a municipalityId or a set of municipalityIds
 * @param {(string|string[])} municipalityId - A municipalityId or set of municipalityIds to retrieve barangays from
 * @returns {string[]} An object array containing all barangays
 */
const getAllBarangays = async (municipalityId) => {
    if (!municipalityId.length) return;
    let barangays = [];

    if (Array.isArray(municipalityId)) {
        for (const _municipalityId of municipalityId) {
            const barangaysFromMun = await fetchData(
                `${endpoint}${_municipalityId}`
            );

            if (!barangaysFromMun) {
                console.log(
                    `[Lazada API] RETRIEVE: Cannot connect to API, ending retrieval at addressId "${_municipalityId}".`
                );
                break;
            } else {
                barangays = barangays.concat(barangaysFromMun);
            }

            // Avoid making too many requests to Lazada API
            // by delaying if the current number of barangay entries are divisible by 5.
            // This virtually introduces a randomly-triggered delay of 1000 milliseconds.
            if (barangays.length && barangays.length % 5 === 0) {
                console.log(`(barangays.length): ${barangays.length}`);
                await delay();
            }
        }
    } else {
        barangays = await fetchData(`${endpoint}${municipalityId}`);
    }

    return barangays;
};

/**
 * Writes to CSV given a dataset and an optional filename
 * @param {*} data - data to be written on the CSV file
 * @param {string} filename - filename of the output CSV file
 */
const writeToCsv = (data, filename = "data.csv") => {
    writeToPath(path.resolve(__dirname, "../output", filename), data, {
        headers: ["id", "name", "parentId"],
    })
        .on("error", () => console.log(`Failed to write '${filename}'`))
        .on("finish", () => console.log(`Done writing to '${filename}'.`));
};

module.exports = {
    delay,
    fetchData,
    getAddressId,
    getAddressIds,
    getAllBarangays,
    writeToCsv,
};
