const { province, endpoint } = require("./constants");
const {
    fetchData,
    getAddressId,
    getAddressIds,
    getAllBarangays,
    writeToCsv,
} = require("./utils");

// Stretch Goal #1: Save all barangays in your province (not just the barangays in your municipality/city)
const stretchGoal_1 = async () => {
    try {
        const provinces = await fetchData(endpoint);
        const provinceId = getAddressId(provinces, province);
        const municipalities = await fetchData(`${endpoint}${provinceId}`);
        const municipalityIds = getAddressIds(municipalities);

        const barangays = await getAllBarangays(municipalityIds);

        writeToCsv(barangays, `data_${province}.csv`);
    } catch (error) {
        console.log("Failed to retrieve data.");
    }
};

// Stretch Goal #2: Save all barangays in the Philippines (not just the barangays in your province or in your municipality/city)
const stretchGoal_2 = async () => {
    try {
        const provinces = await fetchData(endpoint);
        const provinceIds = getAddressIds(provinces);
        let municipalityIds = [];

        for (const provinceId of provinceIds) {
            const municipalities = await fetchData(`${endpoint}${provinceId}`);
            municipalityIds = municipalityIds.concat(
                getAddressIds(municipalities)
            );
        }

        const barangays = await getAllBarangays(municipalityIds);

        writeToCsv(barangays, `data_all.csv`);
    } catch (error) {
        console.log("Failed to retrieve data.");
    }
};

stretchGoal_1();
stretchGoal_2();
