const { endpoint } = require("./constants");
const { delay, fetchData, getAddressIds, getAllBarangays } = require("./utils");
const { insertToDb, retrieveFromDb } = require("./mongoClient");

const cleanUp = (data) =>
    data.map((e) => {
        return { id: e.id, name: e.name, parentId: e.parentId };
    });

const getAddressDetails = async () => {
    try {
        // To avoid multiple requests to Lazada API, we are using data initially stored in MongoDB
        let provinces = await retrieveFromDb("provinces");

        // Fallback if the data is not available in MongoDB yet
        if (!provinces.length) {
            console.log(
                `No items in MongoDB collection "provinces". Using Lazada API instead...`
            );
            provinces = await fetchData(endpoint);
        }
        const provincesQuery = insertToDb("provinces", cleanUp(provinces));
        const provinceIds = getAddressIds(provinces);

        let municipalities = await retrieveFromDb("municipalities");

        // Fallback if the data is not available in MongoDB yet
        if (!municipalities.length) {
            console.log(
                `No items in MongoDB collection "municipalities". Using Lazada API instead...`
            );

            municipalities = [];
            for (const provinceId of provinceIds) {
                const municipalitiesFromProv = await fetchData(
                    `${endpoint}${provinceId}`
                );

                if (!municipalitiesFromProv) {
                    console.log(
                        `[Lazada API] RETRIEVE: Cannot connect to API, ending retrieval at addressId "${_municipalityId}".`
                    );
                    break;
                } else {
                    municipalities = municipalities.concat(
                        municipalitiesFromProvince
                    );
                }

                // Avoid making too many requests to Lazada API
                // by delaying if the current number of barangay entries are divisible by 5.
                // This virtually introduces a randomly-triggered delay of 1000 milliseconds.
                if (municipalities.length && municipalities.length % 5 === 0) {
                    console.log(
                        `(municipalities.length): ${municipalities.length}`
                    );
                    await delay();
                }
            }
        }
        const municipalitiesQuery = insertToDb(
            "municipalities",
            cleanUp(municipalities)
        );
        const municipalityIds = getAddressIds(municipalities);

        const barangays = await getAllBarangays(municipalityIds);
        const barangaysQuery = insertToDb("barangays", cleanUp(barangays));

        console.log("Waiting for all MongoDB queries to resolve...");
        Promise.all([provincesQuery, municipalitiesQuery, barangaysQuery]).then(
            (result) => {
                if (result.every((e) => e === true)) {
                    console.log(
                        "Successfully populated all MongoDB database collections."
                    );
                }
            }
        );
    } catch (error) {
        console.log(error);
        console.log("Failed to retrieve data.");
    }
};

getAddressDetails();
