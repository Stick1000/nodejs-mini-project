const { MongoClient } = require("mongodb");

const mongoDB_config = {
    database: "addresses",
    uri: "mongodb+srv://m001-student:m001-mongodb-basics@sandbox.rdk0a.mongodb.net/addresses?retryWrites=true&w=majority",
    options: {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    },
};

const mongo_client = new MongoClient(
    mongoDB_config.uri,
    mongoDB_config.options
);

/**
 * Inserts a document or multiple documents to the MongoDB database
 * @param {string} collection - the MongoDB collection to store the documents
 * @param {Object|Object[]} documents - an object or array of objects to be stored in the specified collection
 * @returns resolves to `true` upon success, else `false`
 */
const insertToDb = async (collection, documents) => {
    const conn = await mongo_client.connect();
    if (!conn.isConnected() || !documents.length) {
        return false;
    }
    const _collection = conn.db().collection(collection);
    _collection.createIndex({ id: 1 }, { unique: true }); // create unique index on id

    // This try/catch block is necessary because the driver throws an error if there are duplicates
    // or if some documents were not inserted. The exception is caught in order to continue
    // processing the rest of the documents without stalling.
    try {
        if (Array.isArray(documents)) {
            await _collection.insertMany(documents, { ordered: false });
        } else {
            await _collection.insertOne(documents);
        }
    } catch (error) {
        console.log(
            "[MongoDB] INSERT: Some items were not inserted. Possible duplicates."
        );
    } finally {
        console.log(
            `[MongoDB] INSERT: Successfully processed ${documents.length} documents to collection "${collection}"`
        );
        return true;
    }
};

/**
 * Deletes a document or multiple documents to the MongoDB database
 * @param {string} collection - the MongoDB collection to retrieve the documents from
 * @param {Object|Object[]} documents - an object or array of objects to be deleted from the specified collection
 * @returns resolves to `true` upon success, else `false`
 */
const deleteFromDb = async (collection, documents) => {
    const conn = await mongo_client.connect();
    if (!conn.isConnected() || !documents.length) {
        return false;
    }
    const _collection = conn.db().collection(collection);

    if (Array.isArray(documents)) {
        await _collection.deleteMany(documents);
    } else {
        await _collection.deleteOne(documents);
    }

    return true;
};

/**
 * Retrieves all documents from a MongoDB collection.
 * @param {string} collection - the MongoDB collection to retrieve the documents from
 * @returns empty array if connection failed, else an array of documents
 */
const retrieveFromDb = async (collection) => {
    const conn = await mongo_client.connect();
    if (!conn.isConnected()) {
        console.log("[MongoDB] RETRIEVE: Connection failed.");
        return [];
    }
    const _collection = await conn.db().collection(collection).find().toArray();

    return _collection;
};

module.exports = {
    insertToDb,
    deleteFromDb,
    retrieveFromDb,
};
