const province = "Aklan";
const municipality = "Kalibo";

const endpoint =
    "https://member.lazada.com.ph/locationtree/api/getSubAddressList?countryCode=PH&addressId=";

module.exports = {
    province,
    municipality,
    endpoint
};
